# Dovecot
---
Made because I needed a simple secure IMAP-Only dovecot installation.  
It uses virtual users, and provides a **SASL** authentication and **LMTP**.

For adding a user I use a convenient script: **adddovecotuser.sh** *[username]*
  
    IMAP Port: 143
    AUTH Port: 12345 
    LMTP Port: 24 
*Storage by sqlite.*

## Configuration by environment variables (as seen in Dockerfile):

    DOMAIN
    DOVECOT_SSL_CERT_PATH
    DOVECOT_SSL_CERT_KEY
    VMAILUSER_ID
    POSTMASTER

## Mapping
For Mapping these locations are used:

    /var/mail/vhosts 
    /var/mail/dovecot-users 

Naturally extension is always an option by overriding configuration files.

if [ ! $# = 1 ]
 then
  echo "Usage: $0 user"
  exit 1
fi


echo "\nCreate a password for the new email user"
passwd=`doveadm pw -s MD5-CRYPT -u $1`
echo "Adding password for $1@$DOMAIN to /etc/dovecot/dovecot-users"
if [ ! -x /var/mail/dovecot-users ]
 then
  touch /etc/dovecot/dovecot-users
  chmod 640 /var/mail/dovecot-users
fi
echo  "$1@$DOMAIN:$passwd" >> /var/mail/dovecot-users
chgrp dovecot /var/mail/dovecot-users
exit 0

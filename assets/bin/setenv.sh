#!/bin/sh
echo $DOMAIN > /etc/mailname
  
if [ -z "$(getent passwd vmail)" ]; then
  useradd -r -U -u $VMAILUSER_ID vmail -d /var/mail/vhosts -c "virtual mail user"
fi

mkdir -p /var/mail/vhosts
chown -R vmail:vmail /var/mail/vhosts
chgrp dovecot /var/mail/dovecot-users

confset -w 1 ssl_cert=${DOVECOT_SSL_CERT_PATH} ssl_key=${DOVECOT_SSL_CERT_KEY} /etc/dovecot/conf.d/10-ssl.conf
confset -w 1 postmaster_address=${POSTMASTER} /etc/dovecot/conf.d/15-lda.conf

## 10-master.conf: lmtp + auth (sasl) instelling
